#!/bin/bash

SERVER="../server/ipkhttpserver.py"
CLIENT="../client/ipkhttpclient.py"
INTERPRETER="python3"

#PORT="11127"
C="100"
T="20"
PORT=$[ 1100 + $[ RANDOM % 11000 ]] #get random port numver to start server on

if [ `basename "$PWD"` != "test" ]; then
	cd "test"
fi

rm -f ../client/*.header ../client/*.payload ../client/*.log ../server/*.log

echo '-----Testovaci sada spustena-----'

$INTERPRETER $SERVER -c${C} -t${T} -p${PORT} &

sleep 2


$INTERPRETER $CLIENT http://localhost:${PORT}/
echo -e "\nTEST01: Dotaz na adresar, ocekava index.html"
echo "$INTERPRETER $CLIENT http://localhost:${PORT}/"

if diff ../client/ipkResp-*.payload ../server/www/index.html; then
	echo 'OK'
else
	echo 'Fail'
fi

rm -f ../client/*.header ../client/*.payload ../client/*.log ../server/*.log


$INTERPRETER $CLIENT http://localhost:${PORT}/
echo -e "\nTEST02: Pozadavek na soubor"
echo "$INTERPRETER $CLIENT http://localhost:${PORT}/popis.txt"

if diff ../client/ipkResp-*.payload ../server/www/popis.txt; then
	echo 'OK'
else
	echo 'Fail'
fi

rm -f ../client/*.header ../client/*.payload ../client/*.log ../server/*.log


$INTERPRETER $CLIENT http://91.148.0.109:80/chunked.php
echo -e "\nTEST03: Dotaz na jiny server"
echo "$INTERPRETER $CLIENT http://91.148.0.109:80/chunked.php"

if [[ -s ../client/ipkResp-*.payload ]] ; then
	echo 'OK'
else
	echo 'Fail'
fi

rm -f ../client/*.header ../client/*.payload ../client/*.log ../server/*.log





kill -INT $!
