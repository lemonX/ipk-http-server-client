
from optparse import OptionParser
from datetime import datetime
from urllib.parse import urlparse
from time import sleep
import socket
import sys
import re

debug = 0        # debug flag, debug prints...
inLog = None     # income communicatation Log file
outLog = None    # outcome communication Log file
headerf = None   # file to save received header
payloadf = None  # file to save received object
port = None      # port to connect to server
host = None       # server Url address
reqFile = None   # Name of requested file
timeStamp = None # time samp used in names of files

bufSize = 10240  # 10KB

#prints debug messages to stderr if debug flag is set, else do nothing
def print_debug(message):
	if debug == 1:
		print("DEBUG: " + message, file=sys.stderr)


#print err specific message and exit program with code err
def exit_error(err):
	if err == 1:
		print("Unrecognized arguments", file=sys.stderr)
	elif err == 2:
		print("Wrong arguments try --help", file=sys.stderr)
	elif err == 3:
		print("Bad/Not supported URI address", file=sys.stderr)
	elif err == 4:
		print("Cant open log/output file", file=sys.stderr)
	elif err == 5:
		print("Broken pipe", file=sys.stderr)
	else:
		print("Unexpected error", file=sys.stderr)

	sys.exit(err)


#Prints help message to user
def print_help():
	print("Usage: ./ipkhttpclient [-h] URI\n\n"+
	      "-h --help == Help message\n"
	      "URI       == Uniform Resource Identifier, RFC 3986")


#
def makeTimestamp():
	global timeStamp
	timeStamp = datetime.now().strftime('%Y-%m-%d:%H:%M:%S')

#process params, set Globals
def params():
	parser = OptionParser(add_help_option=False)
	parser.add_option("-h", "--help", action="store_true", dest="help", default=False, help="Help message")

	try:
		(options, args) = parser.parse_args()
	except SystemExit:
		exit_error(1) #wrong arguments

	if options.help == True:
		print_help()
		sys.exit(0)

	if len(args) != 1:
		exit_error(2)

	uri = args[0]
	print_debug("URI: " + uri)

	global host, port, reqFile

	o = urlparse(uri)
	if o.scheme != 'http':
		exit_error(3)

	m = re.search('^(.*?):', o.netloc)

	if m:
		host = m.group(1) #if ':' => before ':'
	else:
		host = o.netloc #if not ':' => all

	print_debug("Url: " + host)

	m = re.search(':([0-9]*)', o.netloc)

	if m:
		port = m.group(1) #port specified
	else:
		port = '80' #port not spec, default 80

	print_debug("Port: " + port)

	try:
		port = int(port)
	except:
		exit_error(3)

	reqFile = o.path
	print_debug("File: " + reqFile)


#Generate http GET request to be send to http server
def makeGetReq(reqFile):
	req =  'GET ' + reqFile + " HTTP/1.1\r\n"
	req += 'Host: ' + host + ':' + str(port) + "\r\n"
	req += 'Connecion: keep-alive\r\n'
	req += 'Accept: */*\r\n'
	req += 'User-Agent: ipkhttpclient\r\n'


	req += "\r\n"

	return req


#Opens in|out log file with time stamp
def openLog():
	global inLog, outLog

	inLogFilename = 'ipkHttpClient-' + timeStamp + '.in.log'
	outLogFilename = 'ipkHttpClient-' + timeStamp + '.out.log'

	print_debug("In log file: " + inLogFilename)
	print_debug("Out log file: " + outLogFilename)

	try:
		inLog = open(inLogFilename, 'w', 1) #number 1 means 'line buffered'
		outLog = open(outLogFilename, 'w', 1)
	except:
		exit_error(4) #error open log files


#Opens header|payload file with time stamp
def openOutput():
	global headerf, payloadf

	headerFilename = 'ipkResp-' + timeStamp + '.header'
	payloadFilename = 'ipkResp-' + timeStamp + '.payload'

	print_debug("Header file: " + headerFilename)
	print_debug("Payload file: " + payloadFilename)

	try:
		headerf = open(headerFilename, 'w', 1) #number 1 means 'line buffered'
		payloadf = open(payloadFilename, 'w', 1)
	except:
		exit_error(4) #error open output files


#Close in|out log file
def closeLog():
	global inLog, outLog

	inLog.close()
	outLog.close()

	inLog = None
	outLog = None


#Close header|payload file
def closeOutput():
	global headerf, payloadf

	headerf.close()
	payloadf.close()

	headerf = None
	payloadf = None


#send request to server, receive response, write to files
def communicate():
	response = ''
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host, port))
	
	message = makeGetReq(reqFile)

	try:
		s.sendall(message.encode())
		outLog.write("REQUEST:\n\"\"\"\n" + message + "\n\"\"\"\n\n")
	except:
		exit_error(5)

	#recv in cyclus
	while True:
		try:
			sleep(0.3)
			print_debug("receive...")
			buf = s.recv(bufSize)
		except:
			exit_error(5)

		try:
			buf = buf.decode()
		except:
			print_debug("Decoding error")
			buf = str(buf)

		if buf:
			response += buf
		else:
			break

	response = str(response)

	inLog.write("RESPONSE:\n\"\"\"\n" + response + "\n\"\"\"\n\n")

	s.close()

	return response


#Parse chunk messages and construct original file
#When receive non chunked communication, returns original message
def decodeChunks(message):
	body = ''

	m = re.search('(^.*?\r\n\r\n)(.*)$', message, re.MULTILINE|re.DOTALL)

	if m:
		header = m.group(1)
		chunks = m.group(2)
	else:
		return None, None #bad message format

	#check if chunked
	if re.search('Transfer-Encoding:[\s]*?chunked', header, re.IGNORECASE): #encoding chunked
		while True:
			#print("|" + chunks + "|")
			m = re.match('^([0-9a-fA-F]*)\r\n(.*)', chunks, re.MULTILINE|re.DOTALL)

			size = int(m.group(1), 16)
			chunks = m.group(2)

			body += chunks[:size]
			chunks = chunks[size+2:]

			if not chunks:
				break
	else: #classic encoding
		body = chunks

	return header, body


#Program start
def main():
	makeTimestamp()
	params()
	openLog()

	m = communicate()
	header, message = decodeChunks(m)

	if header and message:
		openOutput()
		headerf.write(header)
		payloadf.write(message)
		closeOutput()
	else:
		exit_error(99)

	closeLog()


main()