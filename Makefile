.PHONY: clean test pack

clean:
	rm -f client/*.header client/*.payload client/*.log server/*.log xmencn00.tgz

test:
	chmod +x test/test.sh
	./test/test.sh

pack:
	tar -zcvf xmencn00.tgz client/ipkhttpclient.py server/ipkhttpserver.py server/www/* test/* Makefile README