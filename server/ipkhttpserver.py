
from optparse import OptionParser
from datetime import datetime
from time import sleep
import socket
import sys
import re
#import socketserver
import signal

debug = 0        # debug flag, debug prints...
chunkSize = None # arg max chunk size
chunkTime = None # arg min chunk time
port = None      # arg port number
inLog = None     # income communicatation Log file
outLog = None    # outcome communication Log file
s = None         # socket

bufSize = 10240  # 10KB
wwwDir = './www' # dir with public data

#prints debug messages to stderr if debug flag is set, else do nothing
def print_debug(message):
	if debug == 1:
		print("DEBUG: " + message, file=sys.stderr)


#print err specific message and exit program with code err
def exit_error(err):
	if err == 1:
		print("Unrecognized arguments", file=sys.stderr)
	elif err == 2:
		print("Wrong arguments try --help", file=sys.stderr)
	elif err == 3:
		print("Expected number as argument", file=sys.stderr)
	elif err == 4:
		print("Cant open log file", file=sys.stderr)
	else:
		print("Unexpected error", file=sys.stderr)

	sys.exit(err)


#Prints help message to user
def print_help():
	print("Usage: ./ipkhttpserver [-h] [-c chunk-max-size] [-p port]\n\n"+
		  "-c --chunk-max-size == Maximal size of data in chunk\n"+
	      "-p --port           == Port number\n"+
	      "-t --min-chunk-time == Mnimum time between 2 chunks\n"+
	      "-h --help           == Help message")


#process params, set Globals
def params():
	parser = OptionParser(add_help_option=False)
	parser.add_option("-c", "--chunk-max-size", dest="chunkMaxSize", help="Maximal size of data in chunk")
	parser.add_option("-p", "--port", dest="port", help="Listen port")
	parser.add_option("-t", "--min-chunk-time", dest="chunkMinTime", help="Mnimum time between 2 chunks")
	parser.add_option("-h", "--help", action="store_true", dest="help", default=False, help="Help message")

	try:
		(options, args) = parser.parse_args()
	except SystemExit:
		exit_error(1) #wrong arguments

	if options.help == True:
		print_help()
		sys.exit(0)

	global chunkSize, port, chunkTime

	if options.chunkMaxSize is not None:
		print_debug("Chunk max size: " + options.chunkMaxSize)
		try:
			chunkSize = int(options.chunkMaxSize)
		except:
			exit_error(3) #number expected
	else:
		print_debug("No chunk size arg")
		exit_error(2) #expected arg

	if options.port is not None:
		print_debug("Port: " + options.port)
		try:
			port = int(options.port)
		except:
			exit_error(3) #number expected
	else:
		print_debug("No port arg")
		exit_error(2) #expected arg

	if options.chunkMinTime is not None:
		print_debug("Chunk min time: " + options.chunkMinTime + "ms")
		try:
			chunkTime = float(options.chunkMinTime) / 1000 #ms to s
		except:
			exit_error(3) #number expected
	else:
		print_debug("No chunk time arg")
		exit_error(2) #expected arg


#Opens in|out log file with time stamp
def openLog():
	global inLog, outLog

	timeStamp = datetime.now().strftime('%Y-%m-%d:%H:%M:%S')

	inLogFilename = 'ipkHttpServer-' + timeStamp + '.in.log'
	outLogFilename = 'ipkHttpServer-' + timeStamp + '.out.log'

	print_debug("In log file: " + inLogFilename)
	print_debug("Out log file: " + outLogFilename)

	try:
		inLog = open(inLogFilename, 'w', 1) #number 1 means 'line buffered'
		outLog = open(outLogFilename, 'w', 1)
	except:
		exit_error(4) #error open log files


#param HTTP request, returns name of requested file
def getFilename(request):
	r = re.compile('^GET (.*?) HTTP/1.1') #catch name of file from request
	m = r.search(request)
	
	if m:
		f = m.group(1)
		if f.endswith('/'): #request for directory
			f += 'index.html' #standard behav. open index.html file
	else:
		f = None

	return f


#Get (GET) request received from client or browser and generate HTTP response to send to client
def makeResponse(request):
	header = ''
	body = ''
	reqFilename = getFilename(request)

	if reqFilename is not None:
		print_debug("Requested file " + wwwDir + reqFilename)

		try:
			with open(wwwDir + reqFilename, 'r') as reqFile:
				body = reqFile.read()
			print_debug("HTTP/1.1 200 OK")
			header = 'HTTP/1.1 200 OK\r\n'
		except:
			print_debug("HTTP/1.1 404 Not Found")
			header = 'HTTP/1.1 404 Not Found\r\n'
			body = 'Error 404, File not found'
	else:
		print_debug("HTTP/1.1 501 Not Implemented")
		header = 'HTTP/1.1 501 Not Implemented\r\n'
		body = 'Error 501, Not Implemented'

	header += 'Content-Type: text/plain\r\n'
	header += 'Transfer-Encoding: chunked\r\n\r\n'

	return header, body


#split response to chunks and sends to socket
def sendChunks(connection, header, body):
	outLog.write("RESPONSE:\n\"\"\"\n" + header)

	try:
		connection.sendall(header.encode())
	except:
		print_debug("Broken pipe")
		return

	#split body to list of chunks
	length = len(body)
	chunks = [ body[i:i+chunkSize] for i in range(0, length, chunkSize)]
	chunks.append('') #final zero length chunk

	#send chunks
	for chunk in chunks:
		c = "%X\r\n%s\r\n" % (len(chunk), chunk)
		print_debug("Chunk sent")
		outLog.write(c)
		sleep(chunkTime)
		try:
			connection.sendall(c.encode())
		except:
			print_debug("Broken pipe")
			return

	outLog.write("\n\"\"\"\n\n")


#Server infinite communication loop
def communicate():
	global s

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(('localhost', port))
	s.listen(10)

	while True:
		print_debug("Waiting for request")
		
		connection, address = s.accept()

		request = connection.recv(bufSize).decode()

		inLog.write("REQUEST:\n\"\"\"\n" + str(request) + "\n\"\"\"\n\n")

		print_debug("REQUEST")

		header, body = makeResponse(request)

		sendChunks(connection, header, body)

		connection.close()


#handle proper server exit after SIGINT signal
def sigintHandler(signum, frame):
	#close log files
	if inLog is not None:
		inLog.close()

	if outLog is not None:
		outLog.close()

	if s is not None:
		s.shutdown(1)
		s.close()

	print_debug("Server exited successfully")
	sys.exit(0)


#Program start
def main():
	signal.signal(signal.SIGINT, sigintHandler) #SIGINT handler
	
	params()
	openLog()
	communicate()


main()